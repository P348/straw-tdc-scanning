
   #CORAL_PATH=../../p348-daq/coral
   CORAL_PATH=/online/soft/p348-daq-straw/p348-daq-straw/coral
     DDD_PATH=$(CORAL_PATH)/src/DaqDataDecoding
     #DDD_PATH=../frolov-07-03-018/DDD/DaqDataDecoding

    #DATE_PATH=../p348-daq-straw/date
    DATE_PATH=../../date
  MNTRNG_PATH=$(DATE_PATH)/monitoring

    ROOT_PATH=/online/soft/root/current
 ROOT_INC_DIR=$(ROOTSYS)/include
 ROOT_LIB_DIR=$(ROOTSYS)/lib

all: get-tdc-data

get-tdc-data: main.cpp
	$(CXX) -g -Wall -std=gnu++0x \
		-DUSE_VOLKOV_DECODING \
		-I$(DDD_PATH)/src $< -o $@ \
                -I$(ROOT_INC_DIR) \
		$(DDD_PATH)/src/libDaqDataDecoding.a \
        $(MNTRNG_PATH)/Linux/libmonitor.a \
	-L$(ROOT_LIB_DIR) \
	-lshift -lexpat \
	-lRint -lRIO -lHist \
	-Wl,-rpath=$(ROOT_LIB_DIR)

clean:
	rm -rf get-tdc-data

.PHONY: all
#/online/soft/p348-daq-straw/p348-daq-straw/coral/src/DaqDataDecoding/src/
