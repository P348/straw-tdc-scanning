import uhal as U
import logging

L = logging.getLogger('app.uhal')

def apply_tdc_cmd( hw, cmd, prefix=[]
                 , readBack=True
                 , dry=False
                 , omitNames=set()
                 , deviceName='' ):
    """
    Recursive procedure that actually sets the uhal parameters.
    Parameters:
      hw uhal's device reference
      cmd the cmd object to apply. In case it is a dictionary, it will be
    iterated with this function recursively
      prefix the array of names to concatenate, to get the actual name
      readBack whether to perform the reading-back procedure
      dry controls whether to perform actual setting. Being set to True causes
    procedure to just print the intentions
      omitNames has to be the Python set object containing strings with
    names of the parameters to ignore within the cfgObj (e.g. 'DAC0').
    """
    ret = None
    if isinstance( cmd, dict ):
        ret = {}
        for k, v in cmd.iteritems():
            if k not in omitNames:
                ret[k] = apply_tdc_cmd( hw, v, prefix + [k] \
                                      , readBack=readBack
                                      , dry=dry
                                      , omitNames=omitNames )
        return ret
    pth = '.'.join(prefix)
    L.debug( "Applying \"%s\" -> %s on device %s."%(pth, str(cmd), deviceName) )
    if not dry:
        hw.getNode(pth).write(cmd)
        hw.dispatch()
        if not readBack:
            return cmd
        rv = hw.getNode(pth).read()
        hw.dispatch()
        L.debug( "  ... read-back: %s"%(rv) )
        return int(rv)
    else:
        return cmd


def apply_tdc_config( tdcID, cfgObj, mgr=None, dry=False
                    , readBack=True, omitNames=set() ):
    #if LoggingMode.verbose == VERB_LVL:
    #    pp = pprint.PrettyPrinter(indent=2)
    #    pp.pprint(cfgObj)
    hw = None
    if not dry:
        hw = mgr.getDevice(tdcID)
        hw.dispatch()
    print tdcID  # XXX
    return apply_tdc_cmd( hw, cfgObj
                        , dry=dry, readBack=readBack
                        , omitNames=omitNames, deviceName=tdcID )

