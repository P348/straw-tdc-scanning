import uhal as U
import yaml, copy, pprint, argparse, sys, os, logging
import subprocess as SP

L = logging.getLogger('app.uhal')

if "__main__" == __name__:
    parser = argparse.ArgumentParser( description="The threshold setting script" \
                                      " for Straws TDC." )
    parser.add_argument("--dry-run", help="Do not perform actual " \
                        "setting of any TDC parameter, but print " \
                        "intentions instead."
                       , action="store_true")
    parser.add_argument("-c", "--conf", help="YAML configuration file path."
                       , type=str, default='conf.yaml')
    parser.add_argument("--connection", help="URI of the connection.xml file " \
                        "used by uhal module."
                       , type=str, default="file://connection.xml")
    parser.add_argument("--no-read-back", help="Won't perform a reading " \
                        "procedure after writing a value.", action='store_true' )
    parser.add_argument("-s", "--scanning-mode", help="Do perform a " \
                        "threshold scanning.", action='store_true')
    parser.add_argument('-v', '--verbosity', help="Verbosity level, from 0 (quiet) " \
                        "to 3 (loquacious).", type=int, default=1)

    args = parser.parse_args()
    manager = None
    dry = args.dry_run
    if not dry:
        U.setLogLevelTo( U.LogLevel.WARNING )  # todo: take from args?
        manager = U.ConnectionManager(args.connection)

    with open(args.conf) as f:
        cfg = yaml.load(f)

    dftTDCCfg = copy.deepcopy(cfg['uhalTDC']['default'])
    if not args.scanning_mode:
        for tdcID in tdcsList:
            tdcCfg = copy.deepcopy( dftTDCCfg )
            if tdcID in cfg['uhalTDC'].keys():
                tdcCfg.update( cfg['uhalTDC'][tdcID] )
            apply_tdc_config( tdcID, tdcCfg, manager \
                    , dry=dry, readBack=(not args.no_read_back) \
                    , omitNames=set(['DAC']) )
        read_data( 'ad-hoc', **cfg['reading'] )
    else:
        from_, to_ = cfg['scanning']['from'], cfg['scanning']['to']
        step = cfg['scanning']['step']
        # Do scan:
        cr = from_
        while cr <= to_:
            L.debug("Configuring threshold=%d..."%cr)
            for tdcID in tdcsList:
                tdcCfg = copy.deepcopy( dftTDCCfg )
                if tdcID in cfg['uhalTDC'].keys():
                    tdcCfg.update( cfg['uhalTDC'][tdcID] )
                tdcCfg.update( { 'DAC' : { 'DAC0' : cr, 'DAC1' : cr } } )
                apply_tdc_config( tdcID, tdcCfg, manager \
                        , dry=dry, readBack=(not args.no_read_back) )
                L.debug("configuring done; reading ...")
            read_data( '%d'%cr, **cfg['reading'] )
            L.debug("reading done.")
            cr += step



