import json, socket, sys, os, struct, logging

L = logging.getLogger('app.net')

def send_msg(sock, msg):
    # Prefix each message with a 4-byte length (network byte order)
    msg = struct.pack('>I', len(msg)) + msg
    sock.sendall(msg)

def recv_msg(sock):
    # Read message length and unpack it into an integer
    raw_msglen = recvall(sock, 4)
    if not raw_msglen:
        return None
    msglen = struct.unpack('>I', raw_msglen)[0]
    # Read the message data
    return recvall(sock, msglen)

def recvall(sock, n):
    # Helper function to recv n bytes or return None if EOF is hit
    data = b''
    while len(data) < n:
        packet = sock.recv(n - len(data))
        if not packet:
            return None
        data += packet
    return data

class Srv:
    def __init__(self, host='', port=50007):
        self.sock = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        L.info("Starting up on \"%s\", port %d"%( host, port ))
        self.sock.bind((host, port))

    def listen( self, actor=None ):
        self.sock.listen(1)
        while True:
            L.debug("Waiting...")
            connection, clientAddr = self.sock.accept()
            try:
                L.debug("Incoming connection from %s."%str(clientAddr))
                data = recv_msg( connection )
                L.debug("Received data of size %d."%len(data))
                if actor:
                    resp = actor( data )
                    send_msg( connection, resp )
            finally:
                connection.close()


class Cli:
    def __init__(self):
        pass

    def send(self, data
            , destHostname='localhost', destPortNo='50007'
            , actor=None ):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.connect((destHostname, destPortNo))
        try:
            send_msg(self.sock, data)
            if actor:
                while True:
                    if actor( recv_msg(self.sock) ):
                        continue
                    break
        finally:
            self.sock.close()
        
        

