import argparse, logging, yaml, copy, pickle, os
from tdcRoutines.net import Cli
import tdcRoutines.logs
import subprocess as SP

L = logging.getLogger('app')


#
# List the detectors IDs here:
#tdcsList = [ 'tdc0', 'tdc1', 'tdc2', 'tdc3' \
#           , 'tdc4', 'tdc5', 'tdc6', 'tdc7' \
#           , 'tdc8', 'tdc9', 'tdcA', 'tdcB' \
#           , 'tdcC' ] #, 'tdcD', 'tdcE', 'tdcF' ]
tdcsList = ['tdc6', 'tdc7']

def read_data( filePrefix
             , util='./get-tdc-data'
             , source='@pcdmre01'
             , mapsDir='./maps'
             , maxSpill=0
             , maxNEvent=0
             , outDir='.'
             , dry=False ):
    if 0 == maxSpill and 0 == maxNEvent:
        sys.stderr.write( 'WARNING: Both DAQ reading stop criteria are disabled'
                ' meaning the reading process will hold execution till source "%s" will be '
                ' depleted.'%source  )
    readArgs = [ util
               , '-i', source
               , '-m', mapsDir
               , '-s', '%d'%maxSpill, '-e', '%d'%maxNEvent
               , '-o', os.path.join(outDir, filePrefix + '.root') ]
    L.info( 'Running reading app w args: %s'%str(readArgs) )
    if not dry:
        p = SP.Popen( readArgs, stderr=SP.PIPE, stdout=SP.PIPE )
        out, err = p.communicate()
        L.debug( out )
        if p.returncode:
            L.error( "Reader process exit with return code %d. Errors log:"%p.returncode )
            L.error( err )
        L.debug( err )


class DryActor(object):
    """Mock client actor doing nothing"""
    def __call__(self, data_):
        data = pickle.loads(data_)
        print str(data) # xxx
        return False  # shall return True to receive additional data

if "__main__" == __name__:
    p = argparse.ArgumentParser(description="")
    p.add_argument( "--server", help="Server hostname."
                  , type=str, default="localhost" )
    p.add_argument( "--port", help="Server port."
                  , type=int, default=50007 )
    p.add_argument("-c", "--conf", help="YAML configuration file path."
                  , type=str, default='conf.yaml')
    args = p.parse_args()

    with open(args.conf) as f:
        cfg = yaml.load(f)

    dftTDCCfg = cfg['uhalTDC']['default']
    from_, to_, step = cfg['scanning']['from'], cfg['scanning']['to'], cfg['scanning']['step']
    cli = Cli()

    cr = from_
    while cr <= to_:
        cfgToApply = {}
        for tdcID in tdcsList:
            tdcCfg = copy.deepcopy( dftTDCCfg )
            if tdcID in cfg['uhalTDC'].keys():
                tdcCfg.update( cfg['uhalTDC'][tdcID] )
            tdcCfg.update({ 'DAC' : { 'DAC0' : cr, 'DAC1' : cr } })
            cfgToApply[tdcID] = tdcCfg
        actor = DryActor()
        cli.send( pickle.dumps(cfgToApply)
                , destHostname=args.server
                , destPortNo=args.port
                , actor=actor  )
        read_data( '%d'%cr, **cfg['reading'] )
        L.info('Done reading.')
        cr += step

