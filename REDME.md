# Straw TDC threshold scanning scripts

## TL;DR

To run the stuff, do

   * on the `pcdmre01` machine (run uhal commands forwarding server):
       $ python ./tdc-fwd-srv.py --port 7707
     keep shell open until everything done.
   * on the `pcdmrc01` machine (run uhal:
       $ . ./.venv/bin/activate
       $ python ./tdc-scan-cli.py --server pcdmre01 --port 7707
       $ root -x build_hst.C  

## Structure

The `main.cpp` contains the C++ code. Corresponding `Makefile` will generate
the `get-tdc-data` application that is designed to accumulate the statistics
from Straw detectors directly from DATE socket supplied by COMPASS/NA64 DAQ
system. This util is further used by python script `tdc-scan-cli.py` that
will schedule thresholds setting.

Since there is no direct access to subnet where TDC setting interfaces are
available, the dedicated script `tdc-fwd-srv.py` must be invoked on
`pcdmre01` machine. It will accept the incoming connections from
`tdc-scan-cli.py` and perform actual `uhal` invocations.

Building the scanning plots is performed by `build_hst.C` script after
scanning procedure is completed yielding fragmentated results at
the `scanned-data/` directory.

## Setting up

Build the `get-tdc-data` application with:

    $ make

Set-up the virtual environment for python client script:

    $ virtualenv ./.venv --prompt="venv-2\n" --system-site-packages

The `--system-sizet-packages` is necessary to support the `uhal` module.

