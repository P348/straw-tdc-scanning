import argparse, uhal, pickle
from tdcRoutines.net import Srv
import tdcRoutines.logs
from tdcRoutines.uhalBindings import apply_tdc_cmd, apply_tdc_config

class UhalDispatchingActor(object):
    """..."""
    def __init__( self
                , dry=False
                , readBack=True
                , omitNames=set()
                , connection='connection.xml' ):
        """
        Parameters:
          param tdcID is expected in form 'tdc0'/'tdc1'/... etc., that uhal may accept.
          cfgObj has to be a Python dictionary
          mgr is an uhal Manager object
          dry will cause the process to not actually dispatch anything to the
        devices, but rather just print the intentions
          readBack defines whether to perform reading parameter after setting.
          omitNames has to be the Python set object containing strings with
        names of the parameters to ignore within the cfgObj (e.g. 'DAC0').
        """
        self.dry = dry
        self.connection = connection
        self.readBack = readBack
        self.omitNames = omitNames

    def __call__(self, data_):
       data = pickle.loads(data_)
       if not isinstance(data, dict):
           raise RuntimeError( 'Got bad data type: %s'%str(type(data)) )
       mgr = None
       if not self.dry:
           uhal.setLogLevelTo( uhal.LogLevel.WARNING )  # todo: configurable?
           mgr = uhal.ConnectionManager(self.connection)
       ret = {}
       for k, v in data.iteritems():
           ret[k] = apply_tdc_config( k, v
                                    , mgr=mgr, dry=self.dry
                                    , readBack=self.readBack
                                    , omitNames=self.omitNames )
       return pickle.dumps(ret)

if "__main__" == __name__:
    p = argparse.ArgumentParser(description="Simple uhal" \
        " command-forwarding server.")
    p.add_argument( "--connection"
                  , help="path to connection.xml file used" \
                    " by uhal module."
                  , default="/online/detector/na64tdc/connection.xml" )
    p.add_argument( "--port", help="port # for incoming connections"
                  , type=int, default=50007 )
    p.add_argument( '--dry', help="Do not actually dispatch instructions" \
                    " to hardware, only print the intentions."
                  , action='store_true' )
    p.add_argument( '--no-read-back', help="Disable reading back" \
                    " parameters after writing.", action='store_true' )
    args = p.parse_args()
    # ...
    actor = UhalDispatchingActor( dry=args.dry
                                , connection=args.connection
                                , readBack=(not args.no_read_back) )
    srv = Srv( port=args.port )
    srv.listen( actor=actor )

