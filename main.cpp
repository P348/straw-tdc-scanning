// TODO: support for Frolov&Uhl's/Volkov's decoding routines has to be removed
// in a future versions of CORAL 4 NA64.
# ifndef USE_VOLKOV_DECODING
#   include "ChipTDC.h"  // Frolov's
#   define CHIP_CLASS ChipTDC
#   define DIGIT_CLASS Digit
# else
#   include "ChipNA64TDC.h"  // Volkov's
#   define CHIP_CLASS ChipNA64TDC
#   define DIGIT_CLASS Digit //DigitRICHPMT
# endif

# include "DaqEventsManager.h"
# include "DaqEvent.h"

# include <TH1F.h>
# include <TH1I.h>
# include <TFile.h>
# include <TDirectory.h>

# include <cstdlib>
# include <iostream>

# include <map>

# include <unistd.h>

static struct AppConfig {
    struct Hst1DParameters {
        Float_t min, max;
        Int_t nBins;
    } amplHstPars
    , timeHstPars
    , chnlHstPars
    ;
} appCfg = {
    // amplitude histo parameters
    { 0, 1.5e3, (int) 3e1 },
    // time histo parameters
    { 0, 1.5e3, (int) 3e1 },
    // channel histo parameters
    { 0, 192, 192 }
};

/// Straw data accumulating container.
struct SDA {
public:
    struct Entry {
        struct Channel {
            TH1F * amplitudes
               , * times
               ;
            Channel( int nCh ) : amplitudes(NULL)
                               , times(NULL)
                               {
                    //std::cout << "Creating new channel " << nCh
                    //          << " at ";  // xxx
                    //gDirectory->pwd();  // xxx
                    char nmBf[32], dscrBf[64];
                    snprintf( nmBf, sizeof(nmBf), "amps-%d", nCh );
                    snprintf( dscrBf, sizeof(dscrBf), "Amplitude distribution for"
                        " %d-th channel", nCh );
                    amplitudes = new TH1F( nmBf, dscrBf
                                         , appCfg.amplHstPars.nBins
                                         , appCfg.amplHstPars.min
                                         , appCfg.amplHstPars.max );
                    snprintf( nmBf, sizeof(nmBf), "times-%d", nCh );
                    snprintf( dscrBf, sizeof(dscrBf), "Time (decoded) distribution for"
                        " %d-th channel", nCh );
                    times = new TH1F( nmBf, dscrBf
                                    , appCfg.timeHstPars.nBins
                                    , appCfg.timeHstPars.min
                                    , appCfg.timeHstPars.max );
            }
            void finalize() {
                amplitudes->Write();
                times->Write();
            }
        };

        /// Channels index.
        std::map<CS::int32, Channel *> channels;
        /// TDirectory where channels histos are stored.
        TDirectory * tDir;
	///
	TH1F * sAmplitudes
           , * sTimes
           ;
        TH1I * sChannels
           ;

        /// Returns channel by its ID. Insert non-existing channel. Note,
        /// that it will ->cd() to the associated TDirectory.
        Channel & operator[]( CS::int32 nCh ) {
            tDir->cd();
            auto it = channels.find( nCh );
            if( channels.end() == it ) {
                auto ch = new Channel( nCh );
                it = channels.insert( std::make_pair(nCh, ch) ).first;
            }
            return *(it->second);
        }

        Entry( TDirectory * dir, CS::DetID dID ) : tDir(dir) {
            dir->cd();
            char nmBf[32], dscrBf[128];
            //
            snprintf( nmBf, sizeof(nmBf), "_ampl-%s", dID.GetName().c_str() );
            snprintf( dscrBf, sizeof(dscrBf), "Amplitudes sum of %s", dID.GetName().c_str() );
            sAmplitudes = new TH1F( nmBf, dscrBf
                                  , appCfg.amplHstPars.nBins
                                  , appCfg.amplHstPars.min
                                  , appCfg.amplHstPars.max );
            //
            snprintf( nmBf, sizeof(nmBf), "_time-%s", dID.GetName().c_str() );
            snprintf( dscrBf, sizeof(dscrBf), "Time sum of %s", dID.GetName().c_str() );
            sTimes = new TH1F( nmBf, dscrBf
                             , appCfg.timeHstPars.nBins
                             , appCfg.timeHstPars.min
                             , appCfg.timeHstPars.max );
            //
            snprintf( nmBf, sizeof(nmBf), "_channels-%s", dID.GetName().c_str() );
            snprintf( dscrBf, sizeof(dscrBf)
                    , "Hits distribution by channels on %s"
                    , dID.GetName().c_str() );
            sChannels = new TH1I( nmBf, dscrBf
                                , appCfg.chnlHstPars.nBins
                                , appCfg.chnlHstPars.min
                                , appCfg.chnlHstPars.max );
        }
    };
    void consider_digit( CS::DetID dID, const CS::Chip::Digit * );
    SDA( const char * outFName );

    void finalize();
private:
    TFile * tf;
    std::map<CS::DetID, Entry> _entries;
};

SDA::SDA( const char * outFName ) {
    tf = new TFile( outFName, "RECREATE" );
}

void
SDA::consider_digit( CS::DetID dID
                   , const CS::Chip::Digit * digit ) {
    tf->cd();
    const CS::CHIP_CLASS::DIGIT_CLASS * tdcDigit =
                dynamic_cast<const CS::CHIP_CLASS::DIGIT_CLASS*>(digit);
    if( !tdcDigit ) return;  // exit: not a TDC digit

    auto it = _entries.find(dID);
    if( _entries.end() == it ) {
        auto newDir = gFile->mkdir( dID.GetName().c_str() );
        auto rc = _entries.insert( std::make_pair(dID, SDA::Entry( newDir, dID )) );
        it = rc.first;
    }
    SDA::Entry & entry = it->second;
    # ifndef USE_VOLKOV_DECODING
    // Prototypes:
    //   int16 GetWire() const
    //   int16 GetChannel() const
    //   int64 GetCoarseTime() const, w/o trigger time subtrd assocd w/ complete TDC in time bins units
    //   int64 GetTime() const, raw time w/o trigger time subtrd assocd w/ channel in time bins units
    //   double GetTimeDecoded() const, ns units
    # error "Frolov's & Uhl's decoding is not supperted."  // TODO
    # else
    // Prototypes:
    //   int32 GetChannel() const
    //   int32 GetWire() const
    //   int32 GetX() const
    //   int32 GetY() const
    //   int32 GetChannelPos() const
    //   int32 GetTime() const
    //   int32 GetAmplitude() const
    //   double GetTimeDecoded() const
    //   float GetTimeReference() const
    entry[ tdcDigit->GetChannel() ]
        .amplitudes->Fill( tdcDigit->GetAmplitude() );
    entry[ tdcDigit->GetChannel() ]
        .times->Fill( tdcDigit->GetTime() );
    entry.sAmplitudes->Fill( tdcDigit->GetAmplitude() );
    entry.sTimes->Fill( tdcDigit->GetTime() );
    entry.sChannels->Fill( tdcDigit->GetChannel() );
    # endif
}

void
SDA::finalize() {
    for( auto it = _entries.begin(); _entries.end() != it; ++it ) {
        for( auto iit = it->second.channels.begin()
           ; it->second.channels.end() != iit; ++iit ) {
            iit->second->finalize();
        }
        it->second.tDir->Write();
        it->second.sAmplitudes->Write();
        it->second.sTimes->Write();
        it->second.sChannels->Write();
    }
    tf->Close();
}

static void
print_usage( const std::string & execName, std::ostream & os ) {
    os << "Usage:\n"
        "    $ " << execName << " -m <map-dir> -i <input-source> -o <out-file>\n"
        "          [-s <max-spills>] [-e <max-events>]\n"
        "Performs read-out of TDCs digits from given source, building\n"
        "few useful distributions. Arguments:\n"
        "  -m <map-dir> -- a dir containing .xml map files for DDD decoding\n"
        "  -i <input-source> -- an input source (file or DDD addr) from where\n"
        "events have to be read. Note, that only single source is supported by\n"
        "now.\n"
        "  -s <max-spills> -- (optional) a maximum number of spills that shall\n"
        "be passed before app will finish.\n"
        "  -e <max-events> -- (optional) a maximum number of event that shall\n"
        "be read befor app app will finish.\n"
        "  -o <out-file> -- output ROOT file where results will be stored.\n"
        ;
}

// argv[1] -- maps dir path
// argv[2] -- data source (e.g. /data/cdr/cdr***.dat or @pcdmWHATEVER)
// ... argv[3..] -- ??? (out files, etc?)
int
main(int argc, char * const argv[]) {
    int opt;
    int nMaxEvents = 0
      , nMaxSpills = 0
      ;
    const char * mapsDir = NULL
             , * inputSource = NULL
             , * outputFileName = NULL
             ;
    int nSpillsProcessed = 0;

    while ((opt = getopt(argc, argv, "m:i:o:s:e:")) != -1) {
        switch (opt) {
            case 's':
                nMaxSpills = atoi(optarg);
            break;
            case 'e':
                nMaxEvents = atoi(optarg);
            break;
            case 'm':
                mapsDir = strdup(optarg);
            break;
            case 'i':
                inputSource = strdup(optarg);
            break;
            case 'o':
                outputFileName = strdup(optarg);
            break;
            default: /* '?' */
                print_usage(argv[0], std::cerr);
                exit(EXIT_FAILURE);
        }
    }

    if( NULL == mapsDir
     || NULL == inputSource
     || NULL == outputFileName ) {
        std::cerr << "At least one of the mandatory arguments is not set."
                  << std::endl;
        print_usage(argv[0], std::cerr);
        exit(EXIT_FAILURE);
    }
    // ... todo: other checks (dir reachable, can create file, etc.)

    CS::DaqEventsManager mgr;
    mgr.SetMapsDir( mapsDir );
    mgr.AddDataSource( inputSource );
    mgr.Print();  // xxx

    SDA acc( outputFileName );

    std::cout << "App configuration:" << std::endl
              << "  maps dir ......... : " << mapsDir << std::endl
              << "  input source ..... : " << inputSource << std::endl
              << "  output file ...... : " << outputFileName << std::endl
              << "Stop conditions (0 means infinite):" << std::endl
              << "  max nEvents ...... : " << nMaxEvents << std::endl
              << "  max spills ....... : " << nMaxSpills << std::endl
              ;


    while( mgr.ReadEvent() ) {
        bool decoded = mgr.DecodeEvent();
        if( ! decoded ) {
            std::cerr << "Event #" << mgr.GetEventsCounter()
                      << " decoding failed. Skipping." << std::endl;
            continue;
        }
        if( !(mgr.GetEventsCounter()%1000) ) {
            std::cout << mgr.GetEventDigits().size() << " digits in event "
                 << mgr.GetEvent().GetRunNumber() << "-"
                 << mgr.GetEvent().GetBurstNumber() << "-"
                 << mgr.GetEvent().GetEventNumberInBurst() << "."
                 << "(" << mgr.GetEventsCounter() << " in source)"
                 << std::endl;
        }
        for( CS::Chip::Digits::const_iterator it = mgr.GetEventDigits().begin()
           ; it != mgr.GetEventDigits().end(); ++ it ) {
            acc.consider_digit( it->first, it->second );
        }
        if( CS::DaqEvent::END_OF_BURST == mgr.GetEvent().GetType() ) {
            ++nSpillsProcessed;
            if( nMaxSpills
             && (nSpillsProcessed >= nMaxSpills) ) {
                std::cout << "Last spill processed w "
                          << mgr.GetEventsCounter()
			  << " events. Interrupt." << std::endl;
                break;
            }
            std::cout << "  ..."
                      << nSpillsProcessed
                      << " spills processed."
                      << std::endl;
        }
        if( nMaxEvents && mgr.GetEventsCounter() >= (unsigned) nMaxEvents ) {
            std::cout << "Last event processed within "
                      << nSpillsProcessed
                      << " spill. Interrupt." << std::endl;
            break;
        }
    }
    acc.finalize();
    std::cout << "Done." << std::endl;
    return EXIT_SUCCESS;
}

